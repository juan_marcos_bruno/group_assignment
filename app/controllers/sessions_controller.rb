class SessionsController < ApplicationController
  def new
    current_user
  end

  def create
    #pull user out of the db and authenticate with password
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in user
      redirectToUserTypePage(user)
    else
      #If doesn't authenticate, redirect to new html (render instruction) and display the error message
      flash[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
  
  private
  
  def redirectToUserTypePage(user)
        if(user.user_type == "Student")
            redirect_to students_path(@get)
        elsif(user.user_type == "Lecturer")
            redirect_to lecturer_path(@get)
        else
            redirect_to admin_path(@get)
        end
  end
  
  
end
