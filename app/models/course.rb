class Course < ApplicationRecord
    validates :name,  presence: true, length: { maximum: 50 }
    validates :semester,  presence: true, length: { maximum: 2 }
    
    
end
