class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users, :id => false do |t|
      t.primary_key :user_id
      t.string :password
      t.string :email
      t.string :user_type
      t.timestamps
    end
  end
end
