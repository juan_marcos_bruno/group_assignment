Rails.application.routes.draw do
  
  #App core mappgins
  get 'sessions/new'
  root 'home#home'
  get 'signup' => 'users#new'
  post 'signup' => 'users#create'
  get    'login' => 'sessions#new'
  post   'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  #Student mappings  
  get 'students' => 'students#new'
  
  #lecturer mappings
  get 'lecturer' => 'lecturer#new'
  post 'lecturer_search_student' => 'lecturer#searchStudent'
  
  #admin mappings
  get 'admin' => 'admin#new'
  get 'admin_create_course' => 'admin#create_course'
  post 'admin_submit_course' => 'admin#submit_course'
  
  resources :users
  #resources :students
end
