class User < ApplicationRecord
     validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
                    
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
    
    has_one :student
    #if need it add for other one to one relationships with the other types of users:
    #has_one :lecturer
    #has_one :admin
    #and their respectives belong_to :user , on each of those linked classes

end
