class UsersController < ApplicationController

    #GET method mapping for /users creates a NEW User object
    def new
        #Create a user model object and redirect to the view that holds the form
        @user = User.new
    end
    
    #POST method mapping, the object created above is persisted in DB here.
    def create
    @user = User.new(user_params) #this needed for validating the form fields values received in the post body request    #@user.type = selectedType    
        #If valid fields, persist to DB
        if @user.save
            create_related_user
          #If saved succesfully, according to the type of user, redirect to the login page, inform before with flash message
         flash[:success] = "You just signed up, please log in!" 
         redirect_to login_path(@get)
        else
          render 'new'
        end
    end
    
    
    private
    #Method handles the calling to the class fields validations
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation, :user_type)
    end
    
    def create_related_user
        if(@user.user_type == "Student")
            #create the student object and save to db
            #assign related application user_id from user just created, create the link between the entities
            student = Student.new
            student.user_id = @user.user_id
            student.save
        end
    end
    
end
