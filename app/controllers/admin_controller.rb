class AdminController < ApplicationController
    #GET ends up here
    def create_course
        render 'create_course'
    end
    
    #POST ends up here: Submit a created course to db, comes from create_course view (create_course.html.erb)
    def submit_course
        if(!(params[:name].blank?) && !(params[:semester].blank?))
        @course = Course.new
        @course.name = params[:name]
        @course.semester = params[:semester]
        @course.lecturer_id = 0
            if @course.save
            #If saved succesfully, according to the type of user, redirect to the login page, inform before with flash message
            flash[:success] = "Course Created!" 
            redirect_to admin_create_course_path(@get)
            end
        else
            flash[:danger] = "Course cannot be created, input correct fields!" 
            redirect_to admin_create_course_path(@get)
        end
        
    end
end
