class LecturerController < ApplicationController
    #Mapping for route post on this resource, on: lecturer#searchStudent @see routes.rb
    #If a search is conducted, for students, from lecturer/new.erb.html, it will hit this method.
    def searchStudent
        #Retrieve students from DB according to search criteria passed from View 
        #(GET->Lecturers#new.html.erb->Post->This controller)
        
        if(!(params[:searchName].blank?))
            #If we have a name inputted, fire the query
            @students = User.where("name LIKE ? and user_type LIKE 'Student'", "%#{params[:searchName]}%")
        elsif(!(params[:searchRegId].blank?))
            #If not we might have a registration id passed
            @students = User.where(" user_id = ? and user_type LIKE 'Student'", params[:searchRegId])
        else
            #Otherwise just display a message of no results found 
            @students = []
        end
        #Render the view for displaying results of model retrieved
        render 'student_search'
    end
end
